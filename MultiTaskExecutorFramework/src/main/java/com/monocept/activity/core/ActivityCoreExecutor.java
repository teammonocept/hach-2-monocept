package com.monocept.activity.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ActivityCoreExecutor {


	private static Log log = LogFactory.getLog(ActivityCoreExecutor.class);
	private int threadPoolSize;
	private static ExecutorService executorService;

	// AtomicInteger provides a int variable which can be read and written atomically for incrementing & decrementing counters.
	private static AtomicInteger idleActivityCount;

	// REALM will start here
	public void startRealm() {/*

		log.info(" Starting Realm Executor Service.. ");

		// Read numOfThreads from DB
		try {
			threadPoolSize = Integer.parseInt(RealmConfigDataUtil.getValue(AppConstants.NUM_OF_THREADS));
		} catch (NumberFormatException n) {
			log.error(" ERROR while converting numberOfThreads.." + n.getMessage());
		}
		idleActivityCount = new AtomicInteger(threadPoolSize);
		log.info("ThreadPoolSize : " + threadPoolSize);

		// Creating thread pool
		try {
			executorService = Executors.newFixedThreadPool(threadPoolSize);
		} catch (Exception e) {
			log.error(" Unable to start executer service " + e);
		}

		// Invoking ActivityPicker using Daemon Thread
		try {
			ActivityPicker activityPicker = new ActivityPicker();
			Thread thread = new Thread(activityPicker);
			thread.setDaemon(true);
			thread.start();
		} catch (Exception e) {
			log.error(" Error in Invoking ActivityPicker " + e);
		}
	*/}

	public static ExecutorService getExecutorService() {

		return executorService;
	}

	public static void incrementIdleActivityCount() {

		idleActivityCount.incrementAndGet();
	}

	public static void decrementIdleActivityCount() {

		idleActivityCount.decrementAndGet();
	}

	public static boolean isActivityIdle() {

		return idleActivityCount.get() > 0 ? true : false;
	}

	public static int getIdleActivityCount() {

		return idleActivityCount.get();
	}

	/*public static void executeNextActivity(ActivityQueueDTO activityQueueDTO) {

		ProcessActivity processActivity = new ProcessActivity(activityQueueDTO);
		getExecutorService().execute(processActivity);
	}*/
	
	/*public static int getActiveActiveCount() {
		int threadSize  = Integer.parseInt(RealmConfigDataUtil.getValue(AppConstants.NUM_OF_THREADS));
		return threadSize - idleActivityCount.get();
		
	
		re}
	*/
	public static void waitTofinishAllActivity()
	{/*
		log.info("Shut Down Triggered");
		ActivityPicker.setThreadPoolActive(false);
		
		while(getActiveActiveCount() != Integer.parseInt(RealmConfigDataUtil.getValue(AppConstants.NUM_OF_THREADS)))
		{
			TimeUtil.delayThread(1000);
			log.info("Waiting to finish all All Active Thread");
		}
		log.info("All Active Thread Finished Theie Work");
	*/}


}
