package com.monocept.activity.restcontroller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActivityRestController {

	Log logger =  LogFactory.getLog(ActivityRestController.class);
	@RequestMapping("/welcome")
	public String sayHello() {
		return "Welcome to Spring Boot : ";
	}
}
