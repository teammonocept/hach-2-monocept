package MultiActivityTaskProject.MultiActivityTaskProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiActivityTaskProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiActivityTaskProjectApplication.class, args);
	}

}
